//
//  LaunchModel.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import Foundation
import UIKit

struct Company: Codable {
    var name: String
    var founder: String
    var founded: Int
    var employees: Int
    var launch_sites: Int
    var valuation: Int
}

struct Launch: Codable {
    var mission_name: String
    var launch_date_local: String
    var launch_date_unix: Int
    var launch_success: Bool?
    var rocket: Rocket
    var links: Links
}

struct Rocket: Codable {
    var rocket_name: String
    var rocket_type: String
}

struct Links: Codable {
    var mission_patch: String?
    var mission_patch_small: String?
    var wikipedia: String?
    var video_link: String?
    
}



//
//  LaunchViewModel.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import Foundation
import UIKit

class LaunchViewModel {
    
    private var companyName: String?
    private var founder: String?
    private var yearFounded: String?
    private var numEmployees: String?
    private var numLaunchSites: String?
    private var valuation: String?
    var launches: [Launch]?

//    MARK: - API Request(s)
    
    private func createCompanyInfoRequest() -> CreateRequest {
        return CreateRequest(httpRequest: .get, endpoint: .info)
    }
    
    func getCompanyDetails() {
        ServiceCall.fetchData(with: createCompanyInfoRequest()) { (response: Result<Company?, Error>) in
            
            switch response {
            case .failure(let err):
                print("COMPANY DETAIL ERROR: \(err)")
            
            case .success(let result):
                guard let data = result else {
                    return
                }
                
                self.companyName = data.name
                self.founder = data.founder
                self.yearFounded = String(data.founded)
                self.numEmployees = String(data.employees)
                self.numLaunchSites = String(data.launch_sites)
                self.valuation = String(data.valuation)
            }
        }
    }
    
    private func companyLaunchRequest() -> CreateRequest {
        return CreateRequest(httpRequest: .get, endpoint: .launches)
    }
    
    func getLaunches(_ completion: @escaping() -> Void) {
        ServiceCall.fetchData(with: companyLaunchRequest()) { (response: Result <[Launch]?,Error>) in
            switch response {
            case .failure(let err):
                print("LAUNCH Error: \(err)")
                
            case .success(let result):
                guard let data = result else {
                    return
                }
                self.launches = data
                
                completion()
            }
            
        }
    }
    
//    MARK: - Sort/Filter
    func sortAscendingByDate(_ launches: [Launch]) -> [Launch] {
        let sortedLaunches = launches.sorted(by: {$0.launch_date_local > $1.launch_date_local})
        
        return sortedLaunches
    }
    
    func sortDescendingByDate(_ launches: [Launch]) -> [Launch] {
        let sortedLaunches = launches.sorted(by: {$0.launch_date_local < $1.launch_date_local})
        
        return sortedLaunches
    }
    
    func findYearsLaunched() -> [String] {
        guard let launches = launches else { return [String]() }
        
        var filteredYears = [String]()
        var yearsLaunched = [SuccessfulLaunches]()

        for index in 0...launches.count-1 {
            let dateString = launches[index].launch_date_local
            let yearString = yearFromDate(dateString)
            if let isSuccess = launches[index].launch_success {
                yearsLaunched.append(SuccessfulLaunches(index: index, year: yearString, didLaunch: isSuccess))
            }
        }

        for index in 0...yearsLaunched.count-1 {
            if index != 0 {
                if yearsLaunched[index].didLaunch != false {
                    if yearsLaunched[index].year != yearsLaunched[index-1].year{
                        filteredYears.append(yearsLaunched[index].year)
                    }
                }
            } else {
                if yearsLaunched[0].didLaunch != false {
                    filteredYears.append(yearsLaunched[0].year)
                }
            }
        }
        
        return filteredYears
    }
        
    struct SuccessfulLaunches {
        var index: Int
        var year: String
        var didLaunch: Bool
    }
    
    func findSuccessfulLaunch(byYear year: String) -> [Launch] {
        guard let launches = launches else { return [Launch]()}
        
        var indexedLaunches = [SuccessfulLaunches]()
        var filteredLaunches = [Launch]()
        
        for index in 0...launches.count-1 {
            let dateString = launches[index].launch_date_local
            let yearString = yearFromDate(dateString)
            if let isSuccess = launches[index].launch_success {
                indexedLaunches.append(SuccessfulLaunches(index: index, year: yearString, didLaunch: isSuccess))
            }
        }
        
        for didLaunch in indexedLaunches {
            if didLaunch.didLaunch != false {
                if didLaunch.year == year {
                    filteredLaunches.append(launches[didLaunch.index])
                }
            }
        }
        
       return filteredLaunches
    }
    
    private func yearFromDate(_ date: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = formatter.date(from: date) else {
            return ""
        }
        
        let yearFormatter = DateFormatter()
        yearFormatter.dateFormat = "yyyy"
        let year = yearFormatter.string(from: date)
        
        return year
    }
    
//    MARK: - Launch Logic/Values
        
    func youtubeUrl(atIndex index: Int) -> String {
        guard let url = launches?[index].links.video_link else { return "" }
        return url
    }
    
    func wikiUrl(atIndex index: Int) -> String {
        guard let url = launches?[index].links.wikipedia else { return "" }
        return url
    }
    
    func rocketImageUrl(_ url: String?) -> String? {
        guard let launchImgUrl = url else { return "" }
        return launchImgUrl
    }
    
    func launchData() -> [Launch] {
        guard let launches = launches else { return [Launch]() }
        return launches
    }
    
    func numberOfLaunches() -> Int {
        guard let launchesCount = launches?.count else { return 0 }
        return launchesCount
    }
    
    func missionName(atIndex index: Int) -> String {
        guard let launchName = launches?[index].mission_name else { return "" }
        return launchName
    }
    
    func rocketName(atIndext index: Int) -> String {
        guard let name = launches?[index].rocket.rocket_name else { return "" }
        return name
    }
    
    func dateTime(atIndex index: Int) -> String {
        guard let launchDate = launches?[index].launch_date_local else { return "" }
        
        return dateTime(fromDate: launchDate)
    }
    
    func daysToFromLaunchTitle(launchDate date: Int) -> String {
        let launchDate = date
        let today = Int(Date().timeIntervalSince1970)
        
        if today >= launchDate {
            return "Days Since Now"
        } else {
            return "Days From Now"
        }
    }
    
    func daysToFromLaunch(launchDate date: String) -> String {
        let launchDate = date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = formatter.date(from: launchDate) else {
            return ""
        }
        
        let today = Date()
        
        let calender = Calendar.current
        let currentDay = calender.startOfDay(for: today)
        let launchDay = calender.startOfDay(for: date)
        let dateComponent = calender.dateComponents([.day], from: launchDay , to: currentDay)
        guard let day = dateComponent.day else { return "" }
        
        return String(day)
    }
    
     func dateTime(fromDate date: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = formatter.date(from: date) else {
            return ""
        }
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        let time = timeFormatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let day = dateFormatter.string(from: date)
        
        return "\(day) at \(time)"
    }
    
    func isLaunchSuccess(_ isSuccess: Bool) -> String {        
        if isSuccess {
            return "✅"
        }
        return "❌"
    }
    
//    MARK: - Return Values: Company info
    func returnCompanyName() -> String {
        if let name = self.companyName {
            return name
        }
        
        return ""
    }
    
    func returnFounder() -> String {
        if let name = self.founder {
            return name
        }
        
        return ""
    }
    
    func returnFounded() -> String {
        if let value = self.yearFounded {
            return value
        }
        
        return ""
    }
    
    func returnNumberOfEmployees() -> String {
        if let value = self.numEmployees {
            return value
        }
        
        return ""
    }
    
    func returnNumberOfLaunchSites() -> String {
        if let value = self.numLaunchSites {
            return value
        }
        
        return ""
    }
    
    func returnValuation() -> String {
        if let value = self.valuation {
            return value
        }
        
        return ""
    }
    
    func companyInfo() -> String {
        return "\(returnCompanyName()) was founded by \(returnFounder()) in \(returnFounded()). It has now \(returnNumberOfEmployees()) employees, \(returnNumberOfLaunchSites()) launch sites, and is valued at USD \(returnValuation())"
    }
}

//
//  LaunchViewController.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import UIKit
import SafariServices

class LaunchViewController: UITableViewController {

    private lazy var viewModel = LaunchViewModel()
    private lazy var imageLoader = ImageViewRemote()
    private var launches = [Launch]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Space X"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(sortData))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchData()
    }
    
    @objc func sortData()  {

        let alertVC = UIAlertController(title: "Filter or Sort", message: "Select sort or filter by specific year", preferredStyle: .actionSheet)
        let yearAlertVC = UIAlertController(title: "Filter By Year", message: "Select year to filter by", preferredStyle: .actionSheet)
        
        
        let yearsLaunched = self.viewModel.findYearsLaunched()
        
        for year in yearsLaunched {
            let year = UIAlertAction(title: year, style: .default, handler: { (action) in
                DispatchQueue.main.async {
                    self.launches = [Launch]()
                    self.launches = self.viewModel.findSuccessfulLaunch(byYear: year)
                    self.tableView.reloadData()
                }
            })
            
            yearAlertVC.addAction(year)
        }
        
        let showYearFilter = UIAlertAction(title: "Filter By Year", style: .default) { (action) in
            self.present(yearAlertVC, animated: true, completion: nil)
        }
        
        let sortAscending = UIAlertAction(title: "Sort By Latest", style: .default) { (action) in
            DispatchQueue.main.async {
                self.launches = self.viewModel.sortAscendingByDate(self.launches)
                self.tableView.reloadData()
            }
        }
        
        let sortDescending = UIAlertAction(title: "Sort By Oldest", style: .default) { (action) in
            DispatchQueue.main.async {
                self.launches = self.viewModel.sortDescendingByDate(self.launches)
                self.tableView.reloadData()
            }
        }
        
        let original = UIAlertAction(title: "Original", style: .default) { (action) in
            DispatchQueue.main.async {
                self.launches = self.viewModel.launchData()
                self.tableView.reloadData()
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(showYearFilter)
        alertVC.addAction(sortAscending)
        alertVC.addAction(sortDescending)
        alertVC.addAction(original)
        alertVC.addAction(cancel)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func fetchData() {
        viewModel.getCompanyDetails()
        
        self.viewModel.getLaunches {
            DispatchQueue.main.async {
                self.launches = self.viewModel.launchData()
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.launches.count
        default:
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let infoCell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as? InfoCell else {
            return UITableViewCell()
        }
        
        guard let launchCell = tableView.dequeueReusableCell(withIdentifier: "launchCell", for: indexPath) as? LaunchCell else {
            return UITableViewCell()
        }
        
        switch indexPath.section {
        case 0:
            infoCell.infoLabel.text = self.viewModel.companyInfo()
            return infoCell

        case 1:
            launchCell.missionNameLabel.attributedText = customAttributeString(withStringName: self.launches[indexPath.row].mission_name, header: "Mission Name")
            launchCell.dateTimeLabel.attributedText = customAttributeString(withStringName: self.viewModel.dateTime(fromDate: self.launches[indexPath.row].launch_date_local), header: "Date/Time")
            launchCell.rocketNameLabel.attributedText = customAttributeString(withStringName: self.launches[indexPath.row].rocket.rocket_name, header: "Rocket Name")
            launchCell.daysLabel.attributedText = customAttributeString(withStringName: self.viewModel.daysToFromLaunch(launchDate: launches[indexPath.row].launch_date_local), header: self.viewModel.daysToFromLaunchTitle(launchDate: launches[indexPath.row].launch_date_unix))
            launchCell.successFailLabel.text = self.viewModel.isLaunchSuccess(launches[indexPath.row].launch_success ?? false)
            launchCell.selectionStyle = .none
            imageLoader.loadImage(fromUrl: self.viewModel.rocketImageUrl(launches[indexPath.row].links.mission_patch_small)) { (image) in
                launchCell.rocketImageView.image = image
            }
            
            return launchCell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Company"
        case 1:
            return "Launches"
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertVC = UIAlertController(title: "More Information", message: "Press to open link", preferredStyle: .actionSheet)
        let wiki = UIAlertAction(title: "Wikipedia", style: .default) { (action) in
            let url = self.viewModel.wikiUrl(atIndex: indexPath.row)
            self.openWebPageInSafari(withURL: url)
        }
        
        let youtube = UIAlertAction(title: "Youtube", style: .default) { (action) in
            let url = self.viewModel.youtubeUrl(atIndex: indexPath.row)
            self.openWebPageInSafari(withURL: url)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(wiki)
        alertVC.addAction(youtube)
        alertVC.addAction(cancel)
        self.present(alertVC, animated: true, completion: nil)
    }

}

extension LaunchViewController: SFSafariViewControllerDelegate {
    func openWebPageInSafari(withURL url: String) {
        guard let launchUrl = URL(string: url) else {
            return
        }
        
        let safariConfig = SFSafariViewController.Configuration()
        safariConfig.entersReaderIfAvailable = true
        safariConfig.barCollapsingEnabled = true
        
        let vc = SFSafariViewController(url: launchUrl, configuration: safariConfig)
        self.present(vc, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LaunchViewController {
    func customAttributeString(withStringName name: String?, header: String) -> NSAttributedString {
        guard let name = name else { return NSAttributedString() }
        let titleString = "\(header): \(name)"
        
        let attributedString = NSMutableAttributedString(string: titleString)
        attributedString.addAttribute(.font, value: ContentFonts.title(), range: getRangeOfSubString(subString: "\(header)", fromString: titleString))
        attributedString.addAttribute(.strokeColor, value: UIColor.gray, range: getRangeOfSubString(subString: "\(header):", fromString: titleString))
        return attributedString
    }
    
    func getRangeOfSubString(subString: String, fromString: String) -> NSRange {
        let sampleLinkRange = fromString.range(of: subString)!
        let startPos = fromString.distance(from: fromString.startIndex, to: sampleLinkRange.lowerBound)
        let endPos = fromString.distance(from: fromString.startIndex, to: sampleLinkRange.upperBound)
        let linkRange = NSMakeRange(startPos, endPos - startPos)
        return linkRange
    }
}

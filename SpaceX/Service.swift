//
//  Service.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import Foundation
import UIKit

enum HTTPRequest: String {
    case get = "GET"
}

enum BaseURL: String {
    case path = "https://api.spacexdata.com/v3/"
}

enum Endpoint: String {
    case info = "info"
    case launches = "launches"
}

protocol Request {
    var endpoint: Endpoint { get }
    var httpRequest: HTTPRequest { get }
}

extension Request {
    func urlRequest() -> URLRequest? {
        let fullURL = URL(string: BaseURL.path.rawValue + endpoint.rawValue)
        guard let unwrappedUrl = fullURL else {
            return nil
        }
        var request = URLRequest(url: unwrappedUrl)
        request.httpMethod = httpRequest.rawValue
        return request
    }
}

struct CreateRequest: Request {
    var httpRequest: HTTPRequest
    
    var endpoint: Endpoint
    
    init(httpRequest: HTTPRequest, endpoint: Endpoint) {
        self.endpoint = endpoint
        self.httpRequest = httpRequest
    }
}


protocol NetworkSession {
    static func fetchData<T: Codable>(with request: Request, completion: @escaping (Result<T?, Error>) -> ())
}

struct ServiceCall: NetworkSession {
    static func fetchData<T>(with request: Request, completion: @escaping (Result<T?, Error>) -> ()) where T : Decodable, T : Encodable {
        
        guard let urlRequest = request.urlRequest() else {
            return
        }
        
        let session = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let recievedData = data else {
                return
            }
            
            do {
                let json = try JSONDecoder().decode(T.self, from: recievedData)
                completion(.success(json))
            } catch let error {
                completion(.failure(error))
            }
        }
        session.resume()
    }
}

struct ImageViewRemote {
    let imageCache = NSCache<NSString, UIImage>()
    
    func loadImage(fromUrl url: String?, completion: @escaping(UIImage) -> ()){
        var currentImageUrl: String?
        
        guard let urlString = url else {
            return
        }
        
        currentImageUrl = urlString
        
        guard let remoteUrl = URL(string: urlString) else {
            return
        }
        
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            completion(imageFromCache)
            return
        }
        
        let session = URLSession.shared.dataTask(with: remoteUrl) { (data, response, error) in
            if error != nil {
                print("Load image error: ", error?.localizedDescription)
                return
            }

            guard let imageData = data else {
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: imageData) {
                    
                    if currentImageUrl == urlString {
                        completion(image)
                    }
                    
                    self.imageCache.setObject(image, forKey: urlString as NSString)
                    completion(image)
                }
            }
        }
        session.resume()
    }
}



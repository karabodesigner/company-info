//
//  LaunchCell.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import UIKit

class LaunchCell: UITableViewCell {

    @IBOutlet weak var missionNameLabel: UILabel! {
        didSet {
            missionNameLabel.font = ContentFonts.body()
            missionNameLabel.textColor = .darkGray
        }
    }
    
    @IBOutlet weak var dateTimeLabel: UILabel! {
        didSet {
            dateTimeLabel.font = ContentFonts.body()
            dateTimeLabel.textColor = .darkGray
        }
    }
    
    @IBOutlet weak var rocketNameLabel: UILabel! {
        didSet {
            rocketNameLabel.font = ContentFonts.body()
            rocketNameLabel.textColor = .darkGray
        }
    }
    
    @IBOutlet weak var daysLabel: UILabel! {
        didSet {
            daysLabel.font = ContentFonts.body()
            daysLabel.textColor = .darkGray
        }
    }
    
    @IBOutlet weak var successFailLabel: UILabel!
    
    @IBOutlet weak var rocketImageView: UIImageView! {
        didSet {
            rocketImageView.contentMode = .scaleAspectFit
        }
    }
    
    override func prepareForReuse() {
        self.rocketImageView.image = nil
    }
    

}

//
//  InfoCell.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            infoLabel.font = ContentFonts.body()
            infoLabel.textColor = .darkGray
        }
    }

}

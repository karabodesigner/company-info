//
//  Helper Functions.swift
//  SpaceX
//
//  Created by Karabo Moloi on 2021/09/06.
//

import Foundation
import UIKit

struct ContentFonts {
    static func body() -> UIFont {
        guard let font = UIFont(name: "Gill Sans", size: 16.0) else {
            return UIFont()
        }
        
        return font
    }
    
    static func title() -> UIFont {
        guard let font = UIFont(name: "Gill Sans SemiBold", size: 16.0) else {
            return UIFont()
        }
        
        return font
    }
    
    static func detail() -> UIFont {
        guard let font = UIFont(name: "Gill Sans Light", size: 16.0) else {
            return UIFont()
        }
        
        return font
    }
}
